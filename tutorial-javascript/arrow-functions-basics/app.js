// Arrow Functions or Fat Arrow Functions
// no name
// no function keyword
// parameters and return statement
//no parameters

// function sayHi() {
//   console.log("hello");
// }
// sayHi();
// const hello = function(name) {
//   console.log(`Hello ${name}`);
// };
// hello("bob");
// one parameter
// function triple(value) {
//   return value * 3;
// }

// const hello = () => console.log("hello");
// hello();

// const double = value => value * 2;
// const num = double(4);
// console.log(num);

// // two parameters and more than one line
// const multiply = (num1, num2) => {
//   const result = num1 * num2;
//   // more code here
//   return result;
// };
// const sum = multiply(3, 4);
// console.log(sum);

// // return object
// const object = () => ({ name: "bob", age: 25 });
// const person = object();
// console.log(person);
// // arrow functions as callback functions
// const numbers = [1, 2, 3, 4, 5, 6];
// const big = numbers.filter(number => number > 2);
// console.log(big);

// const btn = document.querySelector(".btn");
// btn.addEventListener("click", () => console.log("you clicked me"));

// const sayHi = () => console.log("hello");
// sayHi();

// const double = (value) => value * 2;
// const num = double(4);
// console.log(num);

// // two parameters and more than one line
// const multiply = (num1, num2) => {
//     const result = num1 * num2;
//     // more code here
//     return result;
// };

// const sum = multiply(4, 6);
// console.log(sum);

// // return object
// const object = () => ({
//     name: "john",
//     age: 29,
// });

// const person = object();
// console.log(person);

// // arrow functions as callbacks functions
// const numbers = [1, 2, 3, 4, 5, 6];
// const big = numbers.filter((num) => num > 2);
// console.log(big);

// const btn = document.querySelector(".btn");
// btn.addEventListener("click", () => console.log("hello monsieur nseng"));

// default parameters, arrow function gotchas

// sayHi();
// const john = "john";
// const peter = "Peter";

// function sayHi(person = "Susan") {
//     console.log(`Hi ${person}`);
// }

// sayHello();
// const sayHello = (person = "Bob") => console.log(`Hello ${person}`);

// Destructuring
// faster/easier way to access/unpack values from arrays
// objects into variables
// Arrays

// const fruits = ["orange", "banane", "lemon"];
// const friends = ["john", "peter", "bob", "anna", "kelly"];

// const orange = fruits[0];
// const banana = fruits[1];
// const lemon = fruits[2];
// console.log(orange, banana, lemon);

// const [john, peter, bob] = friends;
// console.log(john, peter, bob);

const people = [
    { name: "john", job: "developer" },
    { name: "dimitri", job: "analyst" },
    { name: "wilfried", job: "data scientist" },
];

const constainer = document.querySelector(".container");
const btn = document.querySelector(".btn");

const showPeople = () => {
    const newPeople = people
        .map((person) => {
            // console.log(person);
            const { name, job } = person;
            return `<p>${name} <strong>${job}</strong></p>`;
        })
        .join("");
    // console.log(newPeople);
    constainer.innerHTML = newPeople;
};

btn.addEventListener("click", () => {
    showPeople();
});